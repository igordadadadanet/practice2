/**
 * Задание 1
 *
 * 1. Реализуем приложение Список дел с возможностью добавить новый элемент в список из инпута по нажатию кнопки "Добавить";
 * 2. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
 * 3. Реализовать удаление элемента по нажатию крестика;
 * 4. Реализовать возможность редактирования элемента:
 *    - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить";
 *    - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить".;
 *    - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить".
 *    - Редактируемый элемент подсвечивается как активный.
 * 5. Сохрание текущего состояния.
 */


class Mode {
    static _editMode = false;
    static get editMode() {
        return this._editMode;
    }

    static set editMode(param) {
        if (typeof param !== `boolean`) throw new Error(`передано не булево значение`);
        this._editMode = param;
    }

}


class PageElements {

    listInnerHtmlKey = `listInnerHtml`;
    localStorage = window.localStorage;
    panelInput = document.querySelector(`.js-input`);
    inputClear = document.querySelector(`.input__clear`);
    addButtonEl = document.querySelector('.js-add-button');
    listEl = document.querySelector('.js-list');

}

const page = new PageElements();


class PageStatus extends PageElements {

    loadPreviousState() {
        //загружаем предыдущее состояние
        if (this.localStorage.getItem(this.listInnerHtmlKey)) {
            this.listEl.innerHTML = JSON.parse(this.localStorage.getItem(this.listInnerHtmlKey));
        }
    }

    clearLocalStorage() {
        this.localStorage.clear()
    }

    saveHtmlStatus() {
        //функция сохранения статуса страницы
        this.localStorage.setItem(this.listInnerHtmlKey, JSON.stringify(this.listEl.innerHTML))
    }

    checkboxDisabledEnable() {
        //режим настройки __ включаем отключаем нажатие на чекбоксы
        const allCheckboxes = document.querySelectorAll(`.form-check-input`);
        allCheckboxes.forEach(e => e.disabled = Mode.editMode)
    }

    checkCrossedItems() {
        //проставляем галочки в инпутах с перечеркнутым текстом
        const crossedElements = document.querySelectorAll(`.crossed`);
        if (crossedElements) crossedElements.forEach(e => e.parentNode.querySelector(`.form-check-input`).checked = true);
    }

}

const pageStatus = new PageStatus();
pageStatus.loadPreviousState();
pageStatus.checkCrossedItems();


class WorkingWithElements extends PageStatus {

    clearInput() {
        Mode.editMode = false;
        this.panelInput.value = "";
        this.addButtonEl.innerHTML = `Добавить`;
        if (document.querySelector(`.edit-item`)) document.querySelector(`.edit-item`).classList.remove(`edit-item`);
        this.checkboxDisabledEnable();
    }

    editModeFn(targetText) {
        //режим настройки __ input и кнопка главной панели в разных режимах.
        if (Mode.editMode) {
            this.panelInput.value = targetText;
            this.panelInput.focus();
            this.addButtonEl.innerHTML = `Обновить`;
            if (document.querySelector(`.edit-item`)) document.querySelector(`.edit-item`).classList.remove(`edit-item`);
            this.checkboxDisabledEnable();
        } else {
            this.addButtonEl.innerHTML = `Добавить`;
            document.querySelector(`.edit-item`).innerHTML = this.panelInput.value;
            this.panelInput.value = "";
            if (document.querySelector(`.edit-item`)) document.querySelector(`.edit-item`).classList.remove(`edit-item`);
            this.checkboxDisabledEnable();
        }
    }

    addElement() {
        const inputEl = document.querySelector('.js-input');
        const inputValue = inputEl.value;
        inputEl.value = '';
        ////плодить ли новые объекты?
        const liEl = new ListItem().getItemEl(inputValue);
        this.listEl.append(liEl);
        this.saveHtmlStatus();
    }

    editElement() {
        Mode.editMode = false;
        this.editModeFn();
        this.saveHtmlStatus();
    }

    panelButtonMode() {
        Mode.editMode ? this.editElement() : this.addElement();
    }

    pageListener(event) {
        if (event.target.classList.contains(`js-checkbox`) && !Mode.editMode) {
            event.target.parentNode.querySelector(`.js-item-content`).classList.toggle(`crossed`);
            this.saveHtmlStatus();
        } else if (event.target.classList.contains(`js-remove`) && !Mode.editMode) {
            event.target.parentElement.remove();
            this.saveHtmlStatus();
        } else if (event.target.classList.contains(`js-item-content`)) {
            Mode.editMode = true;
            this.editModeFn(event.target.textContent);
            event.target.classList.add(`edit-item`);
        }
    }

}

const workingWithElements = new WorkingWithElements();


class ListItem {
    liEl = document.createElement('li');
    formCheckEl = document.createElement('div');
    checkBoxEl = document.createElement('input');
    spanEl = document.createElement('span');
    removeBtnEl = document.createElement('div');

    getItemEl(text) {
        this.liEl.classList.add('list-group-item');
        this.formCheckEl.classList.add('form-check');
        this.checkBoxEl.classList.add('form-check-input');
        this.checkBoxEl.classList.add('js-checkbox');
        this.checkBoxEl.setAttribute('type', 'checkbox');
        this.spanEl.classList.add('js-item-content');
        this.spanEl.textContent = text;
        this.removeBtnEl.classList.add('remove');
        this.removeBtnEl.classList.add('js-remove');
        this.removeBtnEl.textContent = 'x';
        this.formCheckEl.append(this.checkBoxEl);
        this.formCheckEl.append(this.spanEl);
        this.liEl.append(this.formCheckEl);
        this.liEl.append(this.removeBtnEl);
        return this.liEl;
    }
}


page.listEl.addEventListener(`click`, workingWithElements.pageListener.bind(workingWithElements));
page.inputClear.addEventListener(`click`, workingWithElements.clearInput.bind(workingWithElements));
page.addButtonEl.addEventListener(`click`, workingWithElements.panelButtonMode.bind(workingWithElements));

/**
 * Задание 2
 *
 * Взломать шифр https://meh-soft.ru
 */


// const arr = document.querySelectorAll(`.form__cipher-input`);
// const submitBtn = document.querySelector(`.form__cipher-button_submit`);
// submitBtn.addEventListener(`click`, ()=> console.log(`click`))
// function safeCrack() {
//     for (let num = 99999; num > 0; num--) {
//         setTimeout(()=> {
//             let numArray = (num + ``);
//             while (numArray.length < arr.length) numArray = `0` + numArray
//             numArray = numArray.split(``)
//             arr.forEach((e, index)=> e.value = numArray[index])
//             submitBtn.click();
//         }, 400)
//
//     }
//     console.log(`done`)
// }

/**
 * Задание 3
 *
 * Сделать скрипт генерации таймера обратного отсчета. Пример вызова:
 * <div id="smart-timer" data-days="1" data-hours="5" data-minutes="30"></div>
 * ID smart-timer помещает во внутрь контейнера верстку с таймером, время таймера задается через опциональные атрибуты.
 */


class Timer {
    constructor(elementID) {
        this.timerDOMElement = document.querySelector(elementID);
        this.dataDays = this.timerDOMElement.getAttribute(`data-days`);
        this.dataHours = this.timerDOMElement.getAttribute(`data-hours`);
        this.dataMinutes = this.timerDOMElement.getAttribute(`data-minutes`);
        this.seconds = 0;

        this.dateTimer;
        this.interval;
    }

    renderTime() {
        this.timerDOMElement.innerHTML = `${this.dataDays} days, ${this.dataHours} hours, ${this.dataMinutes} minutes, ${this.seconds} seconds`;
    }

    setData() {
        const timeLeft = new Date(this.dateTimer.getTime() - new Date().getTime());
        if(timeLeft.getTime() <= 0) {
            this.timerFinish();
        }
        else {
            this.dataDays = timeLeft.getUTCDate() -1;
            this.dataHours = timeLeft.getUTCHours() ;
            this.dataMinutes = timeLeft.getUTCMinutes();
            this.seconds = timeLeft.getUTCSeconds();
            this.setHtmlAttributeValues(this.dataDays, this.dataHours, this.dataMinutes);
        }
    }

    setDateTimer() {
        this.dateTimer = new Date(new Date().getTime() + ((+this.dataMinutes * 60000) + (+this.dataHours * 3600000) + (+this.dataDays * 86400000)));
    }

    setSmartTimerValues({days, hours, minutes}) {
        this.dataDays = days;
        this.dataHours = hours;
        this.dataMinutes = minutes;
        this.setHtmlAttributeValues(days, hours, minutes);
    }

    setHtmlAttributeValues(days, hours, minutes) {
        this.timerDOMElement.setAttribute(`data-days`, days);
        this.timerDOMElement.setAttribute(`data-hours`, hours);
        this.timerDOMElement.setAttribute(`data-minutes`, minutes);
    }

    firstRender() {
        this.setDateTimer();
        this.setData();
        this.renderTime();
    }

    setTimerInterval() {
        this.interval = setInterval(() => {
                this.setData.call(this);
                this.renderTime.call(this);
            }, 100)
    }

    clearTimerInterval() {
        clearInterval(this.interval);
    }

    timerFinish() {
            this.clearTimerInterval();
            alert(`Время вышло`);
    }

    startTimer(obj) {
        if(!this.zeroValidation(obj)) {
            this.setSmartTimerValues(obj);
            this.firstRender();
            this.setTimerInterval();
        }
    }

    zeroValidation(obj) {
        return (+obj.days === 0 && +obj.hours === 0 && +obj.minutes === 0);

    }
}

class TimerHtmlElement {
    constructor(form, timer) {
        this.form = document[form];
        this.timer = timer;
        this.form.addEventListener(`submit`, (event)=> {
            event.preventDefault();
            this.timer.startTimer(this.getValues());
        })
    }

    getValues() {
        return {
            days: this.form.input__days.value,
            hours: this.form.input__hours.value,
            minutes: this.form.input__minutes.value
        }
    }

}
const watch = new TimerHtmlElement(`timerForm`, new Timer(`#smart-timer`));
